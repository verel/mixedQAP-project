/**

   Classe pour représenter une solution à variable mixte:
     vector of double and vector of permutation
     vector de la permutation

*/
public class Solution {
    // vector of real numbers
    public double [] x;

    // permutation
    public int [] sigma;

    // fitness value 
    public double fitness;

    // true when the vector x have been modified
    public boolean modifiedX;

    // personnal flow matrix when x is constant
    public double [] [] flow;

    /***********************************
     * Constructor
     ***********************************/
    public Solution() {
        this.x         = null;
        this.sigma     = null;
        this.fitness   = 0.0;
        this.modifiedX = false;
        this.flow      = null;
    }

    public Solution(int n) {
        this.x = new double[n];
        this.sigma = new int[n];
        this.flow  = new double[n][n];

        this.modifiedX = true;
        this.fitness   = 0.0;
    }

    public Solution(Solution _s) {
        if (_s.x != null) {
            this.x = new double[_s.x.length];
            this.sigma = new int[_s.sigma.length];
            this.flow  = new double[_s.flow.length][_s.flow.length];

            for(int i = 0; i < this.x.length; i++) {
                this.x[i] = _s.x[i];
                this.sigma[i] = _s.sigma[i];
//                this.flow[i]  = new double[_s.flow[i].length];

                for(int j = 0; j < this.flow.length; j++) {
                    this.flow[i][j] = _s.flow[i][j];
                }
            }
        } else {
            this.x = null;
            this.sigma = null;
            this.flow = null;
        }

        this.modifiedX = _s.modifiedX;
        this.fitness = _s.fitness;
    }

    public Solution clone() {
        Solution solution = new Solution(this);

        return solution;
    }

    public void copy(Solution _s) {
        if (_s.x != null) {
            this.x = new double[_s.x.length];
            this.sigma = new int[_s.sigma.length];
            this.flow  = new double[_s.flow.length][_s.flow.length];

            for(int i = 0; i < this.x.length; i++) {
                this.x[i] = _s.x[i];
                this.sigma[i] = _s.sigma[i];

                for(int j = 0; j < this.flow.length; j++) {
                    this.flow[i][j] = _s.flow[i][j];
                }
            }
        } else {
            this.x = null;
            this.sigma = null;
            this.flow = null;
        }

        this.modifiedX = _s.modifiedX;
        this.fitness = _s.fitness;
    }

    /**
     * set the fitness
     */
    public void fitness(double _fit) {
        this.fitness = _fit;
    }
    
    public int size() {
        if (this.x == null) {
            return 0 ;
        } else {
            return x.length;
        }
    }

    /**
     * print the solution
     */
    public String toString() {
        String s = this.fitness + " " ;

        if (this.x == null) {
            s += "" ;
        } else {
            s += this.x.length ;
        
            for(int i = 0; i < this.x.length; i++) {
                s += " " + x[i];
            }

            for(int i = 0; i < this.sigma.length; i++) {
                s += " " + sigma[i];
            }
        }

        return s;
    }

  /**
   * print the flow matrix
   */
  public void printOnFlow() {
      System.out.println(modifiedX);

      for(int i = 0; i < this.sigma.length; i++) {
        System.out.print(flow[i][0]);
        for(int j = 1; j < this.sigma.length; j++) 
            System.out.print(" " + flow[i][j]) ;
        System.out.println();
      }
  }

}

