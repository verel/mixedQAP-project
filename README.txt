Authors: 
  Sébastien Verel

Date: 
  2020/12/30 - Version 1


*** Description:

Please see the project description at: 
https://www.overleaf.com/read/phjcbfrmfycr


*** Instance files:

The directory instances/ contains the set of mixed-variable Quadratic Assignment Problem (mixed-QAP) instance files.


*** Code:

The directory code/src/cpp contains the c++ code. The evaluation function is in the directory code/cpp/src/evaluation, and basic tests are in the directory code/cpp/src/tests.

The directory code/src/java contains the java code. 


*** To compile the code:

In C++:

cd code/cpp
mkdir build
cd build
cmake ../src/tests
make

In java:

cd code/java
javac -d bin -cp bin -sourcepath src -sourcepath src/evaluation src/tests/*.java


*** To execute the tests:

In C++

./t-solution
./t-eval

In JAVA:

java -cp bin Test
